// Load express after installing npm install --save express
var express = require("express");   // loading express module
var app = express();                // creating an express application

/* Use this as the root directory
 __dirname is project folder directory
 All our static files will come from this directory */
app.use(express.static(__dirname + "/public"));

// Start Web Server on port 3000
app.listen(3000,function(){
    console.info("My Web Server has started on port 3000");
    console.info("Document root is at " + __dirname + "/public");
    console.info("Copyright 2016")
});